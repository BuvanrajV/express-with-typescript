"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = require("../models/models"), getUserNames = _a.getUserNames, createUserName = _a.createUserName, updateUserName = _a.updateUserName, deleteUserName = _a.deleteUserName;
var getNamesController = function (req, res) {
    getUserNames()
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({ message: err.message || "Some error occurred." });
    });
};
var createNameController = function (req, res) {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({
            message: "Content can not be empty!!!",
        });
    }
    var newName = {
        name: req.body.name,
    };
    createUserName(newName)
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occurred.",
        });
    });
};
var updateNameController = function (req, res) {
    if (Object.keys(req.body).length === 0) {
        res.status(400).send({
            error: true,
            message: "Content can not be empty!",
        });
    }
    else {
        updateUserName(req.params.id, req.body.name)
            .then(function () {
            return res.send({
                error: false,
                message: "Name successfully updated",
            });
        })
            .catch(function (err) {
            res.status(500).send({
                message: err.message || "Some error occurred.",
            });
        });
    }
};
var deleteNameController = function (req, res) {
    deleteUserName(req.params.id)
        .then(function () {
        return res.send({
            error: false,
            message: "Name Successfully deleted",
        });
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occurred.",
        });
    });
};
module.exports = {
    getNamesController: getNamesController,
    createNameController: createNameController,
    updateNameController: updateNameController,
    deleteNameController: deleteNameController,
};

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mysql2_1 = __importDefault(require("mysql2"));
var config_1 = __importDefault(require("../config"));
var host = config_1.default.host, user = config_1.default.user, password = config_1.default.password, database = config_1.default.database;
var dbConn = mysql2_1.default.createPool({
    connectionLimit: 100,
    host: host,
    user: user,
    password: password,
    database: database,
});
module.exports = dbConn;

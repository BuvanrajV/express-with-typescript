"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var dbConn = require("../database/db");
var getUserNames = function () {
    return new Promise(function (resolve, reject) {
        dbConn.query("SELECT * FROM testAppTable", function (err, res) {
            if (err) {
                console.log("error: ", err);
                reject(err);
            }
            else {
                console.log("names: ", res);
                resolve(res);
            }
        });
    });
};
var createUserName = function (newName) {
    return new Promise(function (resolve, reject) {
        dbConn.query("INSERT INTO testAppTable SET ?", [newName], function (err, res) {
            if (err) {
                console.log("error: ", err);
                reject(err);
            }
            else {
                console.log("created name: ", __assign({}, newName));
                resolve(__assign({}, newName));
            }
        });
    });
};
var updateUserName = function (id, name) {
    console.log(typeof id);
    return new Promise(function (resolve, reject) {
        dbConn.query("UPDATE testAppTable SET name=? WHERE id = ?", [name, id], function (err, res) {
            if (err) {
                console.log("error: ", err);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
};
var deleteUserName = function (id) {
    return new Promise(function (resolve, reject) {
        dbConn.query("DELETE FROM testAppTable WHERE id=?", [id], function (err, res) {
            if (err) {
                console.log("error: ", err);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
};
module.exports = {
    getUserNames: getUserNames,
    createUserName: createUserName,
    updateUserName: updateUserName,
    deleteUserName: deleteUserName,
};

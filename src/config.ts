const dotenv = require("dotenv");
dotenv.config();

const config = {
  dbPort: process.env.DB_PORT,
  host: process.env.HOST,
  user: process.env.DB_USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  port: process.env.PORT || 3010,
};

export default config;

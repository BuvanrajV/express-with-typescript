import { Request, Response } from "express";
const {
  getUserNames,
  createUserName,
  updateUserName,
  deleteUserName,
} = require("../models/models");

const getNamesController = (req: Request, res: Response) => {
  getUserNames()
    .then((data: Response) => {
      res.send(data);
    })
    .catch((err : any) => {
      res.status(500).send({ message: err.message || "Some error occurred." });
    });
};

const createNameController = (req : Request, res: Response) => {
  if (!req.body||Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!!!",
    });
  }
  const newName= {
    name: req.body.name,
  };
  createUserName(newName)
    .then((data: Response) => {
      res.send(data);
    })
    .catch((err : any) => {
      res.status(500).send({
        message: err.message || "Some error occurred.",
      });
    });
};

const updateNameController = (req: Request, res: Response) => {
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      error: true,
      message: "Content can not be empty!",
    });
  } else {
    updateUserName(req.params.id, req.body.name)
      .then(() =>
        res.send({
          error: false,
          message: "Name successfully updated",
        })
      )
      .catch((err : any) => {
        res.status(500).send({
          message: err.message || "Some error occurred.",
        });
      });
  }
};

const deleteNameController = (req: Request, res: Response) => {
  deleteUserName(req.params.id)
    .then(() =>
      res.send({
        error: false,
        message: "Name Successfully deleted",
      })
    )
    .catch((err : any) => {
      res.status(500).send({
        message: err.message || "Some error occurred.",
      });
    });
};

module.exports = {
  getNamesController,
  createNameController,
  updateNameController,
  deleteNameController,
};

import mysql from "mysql2";
import config from "../config";

const { host, user, password, database } = config;
const dbConn = mysql.createPool({
  connectionLimit: 100,
  host: host,
  user: user,
  password: password,
  database: database,
});

module.exports = dbConn;

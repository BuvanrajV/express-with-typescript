import { Request, Response } from "express";
const dbConn = require("../database/db");

const getUserNames = () => {
  return new Promise((resolve, reject) => {
    dbConn.query("SELECT * FROM testAppTable", (err: any, res: Response) => {
      if (err) {
        console.log("error: ", err);
        reject(err);
      } else {
        console.log("names: ", res);
        resolve(res);
      }
    });
  });
};

const createUserName = (newName : {name : string}) => {
  return new Promise((resolve, reject) => {
    dbConn.query(
      "INSERT INTO testAppTable SET ?",
      [newName],
      (err: any, res: Response) => {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          console.log("created name: ", { ...newName });
          resolve({ ...newName });
        }
      }
    );
  });
};

const updateUserName = (id: string, name: string) => {
  console.log(typeof id);
  return new Promise((resolve, reject) => {
    dbConn.query(
      "UPDATE testAppTable SET name=? WHERE id = ?",
      [name, id],
      (err: Error, res: Response) => {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          resolve(res);
        }
      }
    );
  });
};

const deleteUserName = (id: string) => {
  return new Promise((resolve, reject) => {
    dbConn.query(
      "DELETE FROM testAppTable WHERE id=?",
      [id],
      (err: any, res: Response) => {
        if (err) {
          console.log("error: ", err);
          reject(err);
        } else {
          resolve(res);
        }
      }
    );
  });
};

module.exports = {
  getUserNames,
  createUserName,
  updateUserName,
  deleteUserName,
};

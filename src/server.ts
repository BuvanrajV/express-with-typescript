import * as express from "express"
import {Express, Request, Response} from "express";
import routes from "./routes/routes";
import config from "./config";

const app: Express = express.default();

const {port} = config
app.use(express.json());
app.use("/", routes);
app.get("/", (req: Request, res: Response) => {
  res.send(
    "Welcome to CRUD Api : To get Names - /getNames , To post Name - /postName , To update Name - /:id , To delete Name - /:id"
  );
});


app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
